//
//  DataSetTestTests.swift
//  DataSetTestTests
//
//  Created by christina on 5/16/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import XCTest
@testable import DataSetTest

class DataSetTestTests: XCTestCase {
	
	var dataTableTest: DataSet!

    override func setUp() {
		
		dataTableTest = DataSet()
		
    }

    override func tearDown() {
		
		dataTableTest.jobTable = [:]
		
    }

    func testCheckTable() {

		//1. given
		let table = dataTableTest

		//2. where
		let jobsTableTest = table?.jobTable

		//3. then
		XCTAssertNotNil(jobsTableTest)
    }
	
	
	
	func testTableRow() {
		
		//1. given
		var jobsTableTest = dataTableTest.jobTable
		let stepList = ["wearUp" : Step(stepID: "1", stepText: "wear up"),
						"goOut" : Step(stepID: "2", stepText: "go out")]
		jobsTableTest = ["Go to shop" : Job(stepsList: stepList)]
		
		//2. where
		let string = jobsTableTest?["Go to shop"]
		let rowOfstring: [String : Step] = string!.stepsList
		
		
		//3. then
		XCTAssertEqual(rowOfstring.keys, stepList.keys)
		
	}
	

}
