//
//  StepsClass.swift
//  DataSetTest
//
//  Created by christina on 5/16/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import Foundation

class Step {
	
	let stepID: String
	var stepText: String
	
	init(stepID: String, stepText: String) {
		self.stepID = stepID
		self.stepText = stepText
	}
	
}
