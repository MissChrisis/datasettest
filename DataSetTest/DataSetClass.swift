//
//  DataSetClass.swift
//  DataSetTest
//
//  Created by christina on 5/16/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import Foundation

class DataSet {
	
	//MARK: - Properties, create 2 propirties, because we can have steps without job.
	var stepsTable: [Step?] = []
	var jobTable: [String : Job]? = [:]
	
	init() {
		hasChanges()
	}
	
	//MARK: - Methods
	
	func hasChanges() {}
	
	func getChanges() {}
	
	func onRemoveTable() {}
	
	
}

