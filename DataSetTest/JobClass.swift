//
//  JobClass.swift
//  DataSetTest
//
//  Created by christina on 5/16/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import Foundation

class Job {
	
//	let nameJob: String
	var stepsList: [String: Step] = [:]
	
	init(stepsList: [String : Step]) {
//		self.nameJob = nameOfJob
		self.stepsList = stepsList
	}
}
